#[derive(Debug, Clone, PartialEq, Copy)]
pub enum BOperator {
    And, // &
    Or,  // |
    Xor, // ^
    Mat, // >
    Eq,  // =
}

#[derive(Debug, Clone, PartialEq, Copy)]
pub enum UOperator {
    Not,
}

impl BOperator {
    pub fn exec(&self, a: bool, b: bool) -> bool {
        match self {
            BOperator::And => a && b,
            BOperator::Or => a || b,
            BOperator::Xor => a ^ b,
            BOperator::Mat => !(a && !b),
            BOperator::Eq => a == b,
        }
    }
}
impl UOperator {
    pub fn exec(&self, x: bool) -> bool {
        match self {
            UOperator::Not => !x,
        }
    }
}

use std::fmt;
impl fmt::Display for BOperator {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match &self {
                BOperator::And => "'&'",
                BOperator::Or => "'|'",
                BOperator::Xor => "'^'",
                BOperator::Mat => "'>'",
                BOperator::Eq => "'='",
            }
        )
    }
}

impl fmt::Display for UOperator {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match &self {
                UOperator::Not => "'!'",
            }
        )
    }
}
