use crate::ast::{Ast, Node};
use crate::tokenizer::{tokenize, Token};

#[derive(Debug, Clone, PartialEq)]
pub enum ParseError {
    UnaryNoInput,
    BinaryNoInput,
    TooMuchInput,
    TooLitleInput,
}
impl ParseError {
    pub fn message(&self) -> &str {
        match self {
            ParseError::UnaryNoInput => "",
            ParseError::BinaryNoInput => "",
            ParseError::TooMuchInput => "",
            ParseError::TooLitleInput => "",
        }
    }
}

pub fn parse(input: &str) -> Result<(Ast, Vec<char>), ParseError> {
    // Stack to resolve the operators
    let mut stack: Vec<Node> = Vec::new();
    // Variables to add to the list
    let mut vars: Vec<char> = Vec::new();

    // Parse the input string
    for token in tokenize(input) {
        match token {
            Token::UOperator(op) => {
                if stack.len() < 1 {
                    return Err(ParseError::UnaryNoInput);
                }
                let e = stack.pop().unwrap();
                stack.push(Node::Unary {
                    e: Box::new(e),
                    op: op,
                })
            }
            Token::BOperator(op) => {
                if stack.len() < 2 {
                    return Err(ParseError::BinaryNoInput);
                }
                let l = stack.pop().unwrap();
                let r = stack.pop().unwrap();
                stack.push(Node::Binary {
                    l: Box::new(l),
                    r: Box::new(r),
                    op: op,
                })
            }
            Token::Value(b) => stack.push(Node::Bool(b)),
            Token::Id(c) => {
                if !vars.contains(&c) {
                    vars.push(c);
                }
                stack.push(Node::Variable(c))
            }
        }
    }
    // Handle not enough
    if stack.len() > 1 {
        return Err(ParseError::TooMuchInput);
    } else if stack.len() < 1 {
        return Err(ParseError::TooLitleInput);
    }
    vars.sort();
    // Return the parsed Ast
    Ok((
        Ast {
            tree: stack.pop().unwrap(),
        },
        vars,
    ))
}
