use crate::operator::{BOperator, UOperator};
use std::iter::Iterator;

pub enum Token {
    BOperator(BOperator),
    UOperator(UOperator),
    Value(bool),
    Id(char),
}

pub fn tokenize(format: &str) -> impl Iterator<Item = Token> {
    // Allocates a copy of the format to be able to iterate over it.
    let iter = format.chars().collect::<Vec<_>>();
    // Generates the token iterator/stream.
    iter.into_iter().map(|c| match c {
        id @ 'A'..='Z' => Token::Id(id),
        '!' => Token::UOperator(UOperator::Not),
        '&' => Token::BOperator(BOperator::And),
        '|' => Token::BOperator(BOperator::Or),
        '^' => Token::BOperator(BOperator::Xor),
        '>' => Token::BOperator(BOperator::Mat),
        '=' => Token::BOperator(BOperator::Eq),
        // Really this is optional
        '1' => Token::Value(true),
        '0' => Token::Value(false),
        // Default wtf
        _ => panic!("Unexpected input"),
    })
}
