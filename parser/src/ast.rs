use crate::operator::{BOperator, UOperator};
use std::collections::HashMap;

#[derive(Debug, Clone, PartialEq)]
pub enum Node {
    Bool(bool),
    Variable(char),
    Binary {
        op: BOperator,
        l: Box<Node>,
        r: Box<Node>,
    },
    Unary {
        op: UOperator,
        e: Box<Node>,
    },
}

#[derive(Debug, Clone, PartialEq)]
pub struct Ast {
    pub tree: Node,
}

// Syntaxic Result
pub enum Recursion<T> {
    Do(T),
    Dont(T),
}

// Implement display interface
use std::fmt;
impl fmt::Display for Ast {
    fn fmt(&self, _f: &mut fmt::Formatter) -> fmt::Result {
        // This is ugly but I don't know how I'm supposed to use this lib
        // with the writer function
        match ptree::print_tree(&self.tree) {
            Ok(data) => Ok(data),
            Err(_) => Err(std::fmt::Error {}),
        }
    }
}

// Used to print ast as tree
use ptree::{Style, TreeItem};
use std::{borrow::Cow, io};
impl TreeItem for Node {
    type Child = Self;

    fn write_self<W: io::Write>(&self, f: &mut W, _: &Style) -> io::Result<()> {
        let _ = match &self {
            Node::Variable(id) => {
                write!(f, "{}", id)
            }
            Node::Bool(b) => {
                write!(f, "{}", b)
            }
            Node::Binary { op, l: _, r: _ } => {
                write!(f, "{}", op)
            }
            Node::Unary { op, e: _ } => {
                write!(f, "{}", op)
            }
        };
        Ok(())
    }

    fn children(&self) -> Cow<[Self::Child]> {
        let v = match &self {
            Node::Binary { op: _, l, r } => {
                vec![*l.clone(), *r.clone()]
            }
            Node::Unary { op: _, e } => {
                vec![*e.clone()]
            }
            _ => vec![],
        };
        Cow::from(v)
    }
}

impl fmt::Display for Node {
    fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match ptree::print_tree(self) {
            Ok(data) => Ok(data),
            Err(_) => Err(std::fmt::Error {}),
        }
    }
}

pub type ExpansionRules = [fn(&Node) -> Recursion<Node>];
// Actual implementation of tree logic
impl Node {
    pub fn eval(&self, vars: &HashMap<char, bool>) -> bool {
        match self {
            Node::Binary { op, l, r } => op.exec(l.eval(&vars), r.eval(&vars)),
            Node::Unary { op, e } => op.exec(e.eval(vars)),
            Node::Bool(b) => *b,
            Node::Variable(id) => match vars.get(id) {
                Some(value) => *value,
                None => panic!("Couldn't get the value of a variable"),
            },
        }
    }

    pub fn reduce<F>(&self, f: &F) -> Node
    where
        F: Fn(&Node) -> Recursion<Node>,
    {
        // Normal graph recursion
        let node = match self {
            Node::Binary { op, l, r } => {
                let ln = l.reduce(f);
                let rn = r.reduce(f);
                Node::Binary {
                    op: *op,
                    l: Box::new(ln),
                    r: Box::new(rn),
                }
            }
            Node::Unary { op, e } => {
                let en = e.reduce(f);
                Node::Unary {
                    op: *op,
                    e: Box::new(en),
                }
            }
            n => n.clone(),
        };

        fn recur<F: Fn(&Node) -> Recursion<Node>>(f: &F, node: &Node) -> Node {
            match f(node) {
                Recursion::Do(node) => recur(f, &node),
                Recursion::Dont(node) => node,
            }
        }
        recur(f, &node)
    }

    pub fn expand(&self, f: &ExpansionRules) -> Node {
        fn recur(f: &ExpansionRules, node: &Node) -> Node {
            let mut next = node.clone();
            let mut prev = node.clone();
            loop {
                for func in f {
                    // println!("Recur\n{}:{}", next, next == prev);
                    next = match func(&next) {
                        Recursion::Do(node) => recur(f, &node),
                        Recursion::Dont(node) => node,
                    }
                }
                if prev == next {
                    break;
                }
                prev = next.clone()
            }

            next
        }
        let node = recur(f, &self);

        match node {
            Node::Binary { op, l, r } => {
                let ln = l.expand(f);
                let rn = r.expand(f);
                Node::Binary {
                    op,
                    l: Box::new(ln),
                    r: Box::new(rn),
                }
            }
            Node::Unary { op, e } => {
                let en = e.expand(f);
                Node::Unary {
                    op,
                    e: Box::new(en),
                }
            }
            n => n,
        }
    }

    pub fn walk<F, B>(&self, f: &F, acc: &mut Vec<B>)
    where
        F: Fn(&Node) -> Option<B>,
    {
        match f(self) {
            Some(data) => acc.push(data),
            None => {}
        }
        // Walk the graph...
        match self {
            Node::Binary { op: _, l, r } => {
                l.walk(f, acc);
                r.walk(f, acc);
            }
            Node::Unary { op: _, e } => {
                e.walk(f, acc);
            }
            _ => {}
        };
    }
    pub fn into_iter<'a>(&'a self) -> NodeIter<'a> {
        NodeIter {
            right: Vec::new(),
            current: Some(self),
        }
    }
}

#[derive(Debug)]
pub struct NodeIter<'a> {
    right: Vec<&'a Node>,
    current: Option<&'a Node>,
}

// Watch out this doesn't work
impl<'a> Iterator for NodeIter<'a> {
    type Item = &'a Node;
    fn next(&mut self) -> Option<Self::Item> {
        // We get the next thing we need to return
        let next = match self.current {
            Some(v) => Some(v),
            None => self.right.pop(),
        };

        match next {
            Some(n) => match n {
                Node::Binary { op, l: _, r: _ } => println!("op: {}", op),
                Node::Unary { op, e: _ } => println!("op: {}", op),
                Node::Bool(b) => println!("bool: {}", b),
                Node::Variable(v) => println!("var: {}", v),
            },
            _ => {}
        }

        // Prepare for the next one
        self.current = match &next {
            Some(node) => match node {
                Node::Binary {
                    op: _,
                    l: box l,
                    r: box r,
                } => {
                    self.right.push(r);
                    Some(l)
                }
                Node::Unary { op: _, e: box e } => Some(e),
                _ => None,
            },
            None => None,
        };

        next
    }
}

impl Ast {
    pub fn eval(&self, vars: &HashMap<char, bool>) -> bool {
        self.tree.eval(&vars)
    }

    pub fn reduce<F>(&self, f: F) -> Ast
    where
        F: Fn(&Node) -> Recursion<Node>,
    {
        let mut new = self.clone();
        // Recursive reduce
        new.tree = new.tree.reduce::<F>(&f);
        return new;
    }

    pub fn expand(&self, f: &ExpansionRules) -> Ast {
        let mut new = self.clone();
        // Recursive expand
        new.tree = new.tree.expand(f);
        return new;
    }

    pub fn walk<F, B>(&self, f: F) -> Vec<B>
    where
        F: Fn(&Node) -> Option<B>,
    {
        let mut acc: Vec<B> = Vec::new();
        let _ = &self.tree.walk(&f, &mut acc);
        acc
    }

    pub fn into_formula(&self) -> String {
        // Generates the expression string from
        let formula = self.walk(|node| match &node {
            Node::Binary { op, l: _, r: _ } => match op {
                BOperator::And => Some('&'),
                BOperator::Or => Some('|'),
                BOperator::Xor => Some('^'),
                BOperator::Mat => Some('>'),
                BOperator::Eq => Some('='),
            },
            Node::Unary { op, e: _ } => match op {
                UOperator::Not => Some('!'),
            },
            Node::Bool(b) => match b {
                true => Some('1'),
                false => Some('0'),
            },
            Node::Variable(id) => Some(*id),
        });
        formula.into_iter().rev().collect::<String>()
    }

    pub fn iter<'a>(&'a mut self) -> NodeIter<'a> {
        self.tree.into_iter()
    }
}
