use crate::ast::Recursion;
use crate::ast::{Ast, Node};
use crate::operator::{BOperator, UOperator};

use super::simplify::rules_simplify;

pub fn rules_nnf(node: &Node) -> Recursion<Node> {
    match node {
        // A = B : (!A | B) & (A | !B)
        Node::Binary {
            op: BOperator::Eq,
            l,
            r,
        } => Recursion::Do(Node::Binary {
            op: BOperator::And,
            l: Box::new(Node::Binary {
                op: BOperator::Or,
                l: Box::new(Node::Unary {
                    op: UOperator::Not,
                    e: l.clone(),
                }),
                r: r.clone(),
            }),
            r: Box::new(Node::Binary {
                op: BOperator::Or,
                l: Box::new(Node::Unary {
                    op: UOperator::Not,
                    e: r.clone(),
                }),
                r: l.clone(),
            }),
        }),
        // A > B : !A | B
        Node::Binary {
            op: BOperator::Mat,
            l,
            r,
        } => Recursion::Do(Node::Binary {
            op: BOperator::Or,
            l: Box::new(Node::Unary {
                op: UOperator::Not,
                e: l.clone(),
            }),
            r: r.clone(),
        }),
        // A ^ B : (A & !B) | (!A & B)
        Node::Binary {
            op: BOperator::Xor,
            l,
            r,
        } => Recursion::Do(Node::Binary {
            op: BOperator::Or,
            l: Box::new(Node::Binary {
                op: BOperator::And,
                l: Box::new(Node::Unary {
                    op: UOperator::Not,
                    e: Box::new(*l.clone()),
                }),
                r: Box::new(*r.clone()),
            }),
            r: Box::new(Node::Binary {
                op: BOperator::And,
                l: Box::new(Node::Unary {
                    op: UOperator::Not,
                    e: Box::new(*r.clone()),
                }),
                r: Box::new(*l.clone()),
            }),
        }),
        n => Recursion::Dont(n.clone()),
    }
}
pub fn rules_morgan(node: &Node) -> Recursion<Node> {
    match node {
        // Prefix the match because we need to transform only for `not`s
        Node::Unary {
            op: UOperator::Not,
            e: box e, // Deref the box
        } => match e {
            // '!(A & B)' -> '!A | !B'
            Node::Binary {
                op: BOperator::And,
                l,
                r,
            } => Recursion::Do(Node::Binary {
                op: BOperator::Or,
                l: Box::new(Node::Unary {
                    op: UOperator::Not,
                    e: l.clone(),
                }),
                r: Box::new(Node::Unary {
                    op: UOperator::Not,
                    e: r.clone(),
                }),
            }),
            // '!(A | B)' -> '!A & !B'
            Node::Binary {
                op: BOperator::Or,
                l,
                r,
            } => Recursion::Do(Node::Binary {
                op: BOperator::And,
                l: Box::new(Node::Unary {
                    op: UOperator::Not,
                    e: l.clone(),
                }),
                r: Box::new(Node::Unary {
                    op: UOperator::Not,
                    e: r.clone(),
                }),
            }),
            // '!!A' -> 'A'
            Node::Unary {
                op: UOperator::Not,
                e: box e,
            } => Recursion::Do(e.clone()),
            // Apply negation to literal boolean
            Node::Bool(b) => Recursion::Dont(Node::Bool(!b)),
            _ => Recursion::Dont(node.clone()),
        },
        n => Recursion::Dont(n.clone()),
    }
}

pub fn apply_nnf(ast: &Ast) -> Ast {
    // Reduce logical operators to | and & form
    // Spread ! to edges, A.K.A. use De Morgan's law
    ast.expand(&[rules_nnf, rules_morgan, rules_simplify])
}
