use crate::ast::{Ast, Node};
use std::collections::HashMap;

#[derive(PartialEq, Debug, Clone)]
pub struct Truth<Id> {
    variables: Vec<Id>,
    pub values: Vec<bool>,
}

fn to_ids_mapping(expr: &Ast) -> Vec<char> {
    // Iterates over the chars to determine the id index
    // in the gray code
    let mut vars = expr.walk(|node| match node {
        Node::Variable(c) => Some(*c),
        _ => None,
    });
    vars.sort();
    vars.dedup();
    vars
}

impl std::fmt::Display for Truth<char> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        // Print the header table
        for var in &self.variables {
            write!(f, "| {} ", var)?;
        }
        writeln!(f, "| = |")?;
        // Print the line separator
        for _ in &self.variables {
            write!(f, "|---")?;
        }
        writeln!(f, "|---|")?;

        let size = &self.variables.len();
        // Prints all the permutation rows
        for state in 0..(1 << size) {
            // Loops on all the vars
            for (idx, _) in self.variables.iter().enumerate() {
                let val = state & (1 << (size - idx - 1));
                write!(f, "| {} ", if val > 0 { 1 } else { 0 })?;
            }
            let value = (1 << size) - state - 1;
            writeln!(f, "| {} |", if self.values[value] { 1 } else { 0 })?;
        }

        Ok(())
    }
}

impl Truth<char> {
    pub fn new(vars: &Vec<char>, expr: &Ast) -> Truth<char> {
        let size = &vars.len();
        let mut states: Vec<bool> = Vec::with_capacity(1 << size);

        // Loops on all the different permutations for the table
        for state in (0..(1 << size)).rev() {
            // Maps id to values for running the table evaluation
            let mut values: HashMap<char, bool> =
                HashMap::with_capacity(1 << size);
            // Loops on all the vars
            for (idx, id) in vars.iter().enumerate() {
                values.insert(*id, state & (1 << (size - idx - 1)) > 0);
            }
            let res = expr.eval(&values);
            states.push(res);
        }

        // Return the truth table
        Truth {
            variables: vars.clone(),
            values: states,
        }
    }
    pub fn from(expr: &Ast) -> Truth<char> {
        Self::new(&to_ids_mapping(expr), expr)
    }
}
