use crate::ast::Recursion;
use crate::ast::{Ast, Node};
use crate::operator::BOperator;

use super::nnf::{rules_morgan, rules_nnf};
use super::simplify::rules_simplify;

type ExpansionRule = fn(&Node) -> Recursion<Node>;
const RULES: &[ExpansionRule] =
    &[rules_cnf, rules_nnf, rules_morgan, rules_simplify];

pub fn rules_cnf(node: &Node) -> Recursion<Node> {
    match node {
        // 'A | (B & C)' or '(B & C) | A' -> '(B | A) & (C | A)'
        Node::Binary {
            op: BOperator::Or,
            l:
                box Node::Binary {
                    op: BOperator::And,
                    l: b,
                    r: c,
                },
            r: a,
        }
        | Node::Binary {
            op: BOperator::Or,
            l: a,
            r:
                box Node::Binary {
                    op: BOperator::And,
                    l: b,
                    r: c,
                },
        } => {
            // println!("found:\n{}", n);
            Recursion::Do(Node::Binary {
                op: BOperator::And,
                l: Box::new(Node::Binary {
                    op: BOperator::Or,
                    r: a.clone(),
                    l: b.clone(),
                }),
                r: Box::new(Node::Binary {
                    op: BOperator::Or,
                    r: a.clone(),
                    l: c.clone(),
                }),
            })
        }
        // Here we do nothing but clone the tree
        _ => Recursion::Dont(node.clone()),
    }
}

pub fn apply_cnf(ast: &Ast) -> Ast {
    // Reduce logical operators to | and & form
    let mut next = ast.clone();
    let mut prev = ast.clone();
    loop {
        next = next.expand(RULES);
        if next == prev {
            break;
        }
        prev = next.clone();
    }
    next
}

#[cfg(test)]
mod test {
    use super::apply_cnf;
    use crate::parser::parse;

    #[test]
    fn cnf_xor() {
        let (rpn, _rpn_vars) = match parse("AB^") {
            Ok(data) => data,
            Err(_) => panic!("Couldn't parse ast"),
        };
        let cnf = apply_cnf(&rpn);
        println!("cnf:{}\n{}", cnf.into_formula(), cnf);
    }
}
