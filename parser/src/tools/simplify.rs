use crate::ast::Recursion;
use crate::ast::{Ast, Node};
use crate::operator::{BOperator, UOperator};

pub fn rules_simplify(node: &Node) -> Recursion<Node> {
    match node {
        // 'A | 1' -> '1'
        Node::Binary {
            op: BOperator::Or,
            l: _,
            r: box Node::Bool(true),
        }
        | Node::Binary {
            op: BOperator::Or,
            l: box Node::Bool(true),
            r: _,
        } => Recursion::Dont(Node::Bool(true)),
        // 'A | 0' -> 'A'
        Node::Binary {
            op: BOperator::Or,
            l: box e,
            r: box Node::Bool(false),
        }
        | Node::Binary {
            op: BOperator::Or,
            l: box Node::Bool(false),
            r: box e,
        } => Recursion::Dont(e.clone()),
        // 'A | A' -> 'A'
        Node::Binary {
            op: BOperator::Or,
            l: box a,
            r: box b,
        } if a == b => Recursion::Dont(a.clone()),

        // 'A & 0' -> '0'
        Node::Binary {
            op: BOperator::And,
            l: _,
            r: box Node::Bool(false),
        }
        | Node::Binary {
            op: BOperator::And,
            l: box Node::Bool(false),
            r: _,
        } => Recursion::Dont(Node::Bool(false)),
        // 'A & 1' -> 'A'
        Node::Binary {
            op: BOperator::And,
            l: box e,
            r: box Node::Bool(true),
        }
        | Node::Binary {
            op: BOperator::And,
            l: box Node::Bool(true),
            r: box e,
        } => Recursion::Dont(e.clone()),
        // 'A & A' -> 'A'
        Node::Binary {
            op: BOperator::And,
            l: box a,
            r: box b,
        } if a == b => Recursion::Dont(a.clone()),

        // '!A | A' -> '1'
        Node::Binary {
            op: BOperator::Or,
            l:
                box Node::Unary {
                    op: UOperator::Not,
                    e: l,
                },
            r,
        }
        | Node::Binary {
            op: BOperator::Or,
            l,
            r:
                box Node::Unary {
                    op: UOperator::Not,
                    e: r,
                },
        } if l == r => Recursion::Do(Node::Bool(true)),

        // // '!A & A' -> '0'
        Node::Binary {
            op: BOperator::And,
            l:
                box Node::Unary {
                    op: UOperator::Not,
                    e: l,
                },
            r,
        }
        | Node::Binary {
            op: BOperator::And,
            l,
            r:
                box Node::Unary {
                    op: UOperator::Not,
                    e: r,
                },
        } if l == r => Recursion::Do(Node::Bool(false)),

        // '!!A' -> 'A'
        Node::Unary {
            op: UOperator::Not,
            e:
                box Node::Unary {
                    op: UOperator::Not,
                    e: box e,
                },
        } => Recursion::Do(e.clone()),

        // Do nothing
        n => Recursion::Dont(n.clone()),
    }
}

pub fn simplify(ast: &Ast) -> Ast {
    // Reduce the ast to a simpler version
    ast.expand(&[rules_simplify])
}

#[cfg(test)]
mod test {
    use crate::parser::parse;
    use crate::tools::cnf::apply_cnf;
    use crate::tools::nnf::apply_nnf;
    use crate::tools::simplify::simplify;
    use crate::tools::truth::Truth;

    const EXPRS: &[&str] = &[
        // Simple binop tests
        // "AB|",
        // "AB&",
        "AB^",
        // "AB=",
        // "AB>",
        // Simple pruning tests
        // "AA|",
        // "AA&",
        // Simple propagation tests
        // "AB|!",
        // "AB^!",
        // "AB&!",
        // Advanced tests
        // "AB>C|A!&D=!",
        // Hardcore tests
        // "ABC||!",
        // "ABC&|!",
        // "ABC|&!",
        // "ABC&&!",
        // "A!B=C!|D&E!|F^!",
    ];

    #[test]
    fn truth_comp() {
        for test in EXPRS {
            let (rpn, rpn_vars) = match parse(test) {
                Ok(data) => data,
                Err(_) => panic!("Couldn't parse ast"),
            };
            let nnf = apply_nnf(&rpn);
            let cnf = apply_cnf(&nnf);
            let simple = simplify(&cnf);
            let trpn = Truth::new(&rpn_vars, &rpn);
            let tsimple = Truth::new(&rpn_vars, &simple);
            println!(
                "Rpn\n{}\nSimple\n{}",
                rpn.into_formula(),
                simple.into_formula()
            );
            println!("NNF\n{}\nCNF\n{}\nSimple\n{}", nnf, cnf, simple);
            assert_eq!(
                trpn, tsimple,
                "Truth tables are not the same.\n{}\n{}",
                trpn, tsimple
            );
        }
    }
}
