use boole::truth::print_truth_table;
use std::env;

fn main() {
    for arg in env::args().nth(1) {
        print_truth_table(&*arg);
    }
}
