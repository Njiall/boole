use parser::{ast::Node, parser::parse, tools::cnf::apply_cnf};

fn sat(formula: &str) {
    // Parse the input format
    let (mut ast, _) = match parse(formula) {
        Ok(data) => data,
        Err(err) => panic!("{:?}", err),
    };
    println!("ast:\n{}", ast);
    let new = ast.iter().map(|node| node).collect();
    println!("new:\n{}", new);
    // let cnf = apply_cnf(&ast);
    // println!("cnf:\n{}", cnf);
}

use std::env;
fn main() {
    for arg in env::args().skip(1) {
        sat(&arg)
    }
}
