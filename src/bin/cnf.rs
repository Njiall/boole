use parser::{
    parser::parse,
    tools::{cnf::apply_cnf, nnf::apply_nnf, truth::Truth},
};
use std::env;

fn main() {
    for arg in env::args().skip(1) {
        let (ast, _) = match parse(&arg) {
            Ok(data) => data,
            Err(err) => panic!("{:?}", err),
        };
        let cnf = apply_cnf(&ast);
        let nnf = apply_nnf(&ast);
        // Generates the formula from ast
        let formula_nnf = nnf.into_formula();
        let formula_cnf = cnf.into_formula();
        let table = Truth::from(&cnf);

        println!("rpn expr: {}\n{}", arg, ast);
        println!("nnf expr: {}\n{}", formula_nnf, nnf);
        println!("cnf expr: {}\n{}", formula_cnf, cnf);
        println!("{}", table);
    }
}
