use boole::powerset::powerset;
use std::env;
fn main() {
    for arg in env::args().skip(1) {
        // Parse input to Vec<i32>
        let set: Vec<i32> = arg
            .split_whitespace()
            .map(|s| s.parse().expect("parse error"))
            .collect();
        // Prints the powerset of the input
        println!("{:?}", powerset(&set[..]));
    }
}
