use boole::nnf::nnf;
use std::env;

fn main() {
    for arg in env::args().skip(1) {
        println!("rpn expr: {}", arg);
        println!("nnf expr: {}", nnf(&*arg));
    }
}
