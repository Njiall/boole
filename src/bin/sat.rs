use parser::parser::parse;
use parser::tools::truth::Truth;

fn sat(formula: &str) -> bool {
    // Parse the input format
    let (ast, vars) = match parse(formula) {
        Ok(data) => data,
        Err(err) => panic!("{:?}", err),
    };
    Truth::new(&vars, &ast).values.contains(&true)
}

use std::env;
fn main() {
    for arg in env::args().skip(1) {
        println!("{}: {}", &*arg, sat(&arg))
    }
}
