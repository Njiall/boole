use boole::multiply::multiply;
use std::env;
fn main() {
    for arg in env::args().skip(1) {
        let nums: Vec<u32> = arg
            .split_whitespace()
            .map(|a| a.parse().expect("parse error"))
            .collect();
        assert_eq!(nums.len(), 2);
        println!("{}", multiply(nums[0], nums[1]));
    }
}
