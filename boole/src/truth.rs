// use crate::bool_eval::eval_formula;
// use crate::gray_code::gray_code;
use crate::bool_eval::eval_formula;
use std::collections::HashMap;

fn to_ids_mapping(format: &str) -> Vec<char> {
    // As we can have only 26 ids max we use it
    let mut ids: Vec<char> = Vec::with_capacity(26);
    // Iterates over the chars to determine the id index
    // in the gray code
    for char in format.chars() {
        match char {
            c @ 'A'..='Z' => {
                if !ids.contains(&c) {
                    ids.push(c);
                }
            }
            _ => {}
        }
    }

    return ids;
}

use std::io::stdout;
use termion::{color, is_tty};
#[allow(dead_code)]
pub fn print_truth_table(original: &str) {
    // Gets the ids with their positions
    let ids = to_ids_mapping(original);

    // Prints the table head
    for id in &ids {
        print!("| {} ", id);
    }
    println!("| = |");
    for _ in &ids {
        print!("|---");
    }
    println!("|---|");

    // Prints all the rows of the table
    for idx in 0..(1 << ids.len()) {
        let mut formula = original.to_owned();
        let mut values: HashMap<char, bool> = HashMap::new();

        // Maps all the ids to the values for the row
        for &id in &ids {
            let mut index = ids.iter().position(|&x| x == id).unwrap();
            // Reverse the id for the right position
            index = ids.len() - index - 1;
            values.insert(id, idx & (1 << index) > 0);
        }

        // Prints the values for the mapped ids
        for id in &ids {
            if is_tty(&stdout()) {
                if values[&id] {
                    print!("| {}1{} ", color::Fg(color::Green), color::Fg(color::Reset));
                } else {
                    print!("| {}0{} ", color::Fg(color::Red), color::Fg(color::Reset));
                }
            } else {
                print!("| {} ", if values[&id] { 1 } else { 0 });
            }
        }

        // Replace all ids with their current value
        formula = formula
            .chars()
            .map(|c| match c {
                id @ 'A'..='Z' => {
                    if values[&id] == true {
                        '1'
                    } else {
                        '0'
                    }
                }
                _ => c,
            })
            .collect();

        // Prints the formula result
        if is_tty(&stdout()) {
            if eval_formula(&*formula) {
                println!(
                    "| {}1{} |",
                    color::Fg(color::Green),
                    color::Fg(color::Reset)
                );
            } else {
                println!("| {}0{} |", color::Fg(color::Red), color::Fg(color::Reset));
            }
        } else {
            println!("| {:?} |", eval_formula(&*formula) as u8);
        }
    }
}
