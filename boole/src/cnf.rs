use parser::parser::parse;
use parser::tools::cnf::apply_cnf;

pub fn cnf(format: &str) -> String {
    // Parse the input format
    let (ast, _) = match parse(format) {
        Ok(data) => data,
        Err(err) => panic!("{:?}", err),
    };
    let cnf = apply_cnf(&ast);
    // Generates the formula from ast
    cnf.into_formula()
}

#[cfg(test)]
mod test {
    use crate::cnf::cnf;
    use parser::parser::parse;
    use parser::tools::truth::Truth;

    const EXPRS: &[&str] = &[
        // Simple binop tests
        "AB|",
        "AB&",
        "AB^",
        "AB=",
        "AB>",
        // Simple pruning tests
        "AA|",
        "AA&",
        // Simple propagation tests
        "AB|!",
        "AB^!",
        "AB&!",
        // Advanced tests
        "AB>C|A!&D=!",
        // Hardcore tests
        "ABC||!",
        "ABC&|!",
        "ABC|&!",
        "ABC&&!",
        "A!B=C!|D&E!|F^!",
    ];

    #[test]
    fn truth_comp() {
        for test in EXPRS {
            let (rpn, rpn_vars) = match parse(test) {
                Ok(data) => data,
                Err(_) => panic!("Couldn't parse ast"),
            };
            let (cnf, cnf_vars) = match parse(&cnf(test)) {
                Ok(data) => data,
                Err(_) => panic!("Couldn't parse ast"),
            };
            let trpn = Truth::new(&rpn_vars, &rpn);
            let tcnf = Truth::new(&cnf_vars, &cnf);
            assert_eq!(
                trpn, tcnf,
                "Truth tables are not the same.\n{}\n{} for equation: {}",
                trpn, tcnf, test
            );
        }
    }
}
