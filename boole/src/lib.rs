// Binary operators
pub mod adder;
pub mod multiply;
// Boolean algebra
pub mod bool_eval;
pub mod gray_code;
pub mod truth;
// Boolean expression modifications
pub mod cnf;
pub mod nnf;
// Set theory
pub mod powerset;
