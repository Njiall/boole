// Function that takes a mathematical formula
// and evaluates it's truth
//
// 0 ⊥ false
// 1 > true
// ! ¬ Negation
// & ∧ Conjunction
// | ∨ Disjunction
// ˆ ⊕ Exclusive disjunction
// > ⇒ Material condition
// = ⇔ Logical equivalence
#[allow(dead_code)]
pub fn eval_formula(formula: &str) -> bool {
    let mut stack: Vec<bool> = vec![];

    for token in formula.chars() {
        match token {
            // Operands
            '0' => {
                stack.push(false);
            }
            '1' => {
                stack.push(true);
            }
            // Operators
            '!' => {
                // Negation
                let a = stack.pop().expect("missing operand");
                stack.push(!a);
            }
            '&' => {
                // Conjunction
                let b = stack.pop().expect("missing operand 1");
                let a = stack.pop().expect("missing operand 2");
                stack.push(a && b);
            }
            '|' => {
                // Disjunction
                let b = stack.pop().expect("missing operand 1");
                let a = stack.pop().expect("missing operand 2");
                stack.push(a || b);
            }
            '^' => {
                // Exclusive disjunction
                let b = stack.pop().expect("missing operand 1");
                let a = stack.pop().expect("missing operand 2");
                stack.push(a ^ b);
            }
            '>' => {
                // Material conditional
                let b = stack.pop().expect("missing operand 1");
                let a = stack.pop().expect("missing operand 2");
                stack.push(!(a && !b));
            }
            '=' => {
                // Logical equivalence
                let b = stack.pop().expect("missing operand 1");
                let a = stack.pop().expect("missing operand 2");
                stack.push(a == b);
            }
            _ => {
                panic!("unknow character from the set")
            }
        }
    }
    if stack.len() > 1 {
        panic!("too much input");
    }
    return stack.pop().expect("missing result");
}

#[test]
fn test_eval_formula() {
    const TESTS: &[(&str, bool)] = &[
        // And truth table
        ("00&", false),
        ("01&", false),
        ("10&", false),
        ("11&", true),
        // Or truth table
        ("00|", false),
        ("10|", true),
        ("10|", true),
        ("11|", true),
        // Xor truth table
        ("00^", false),
        ("10^", true),
        ("10^", true),
        ("11^", false),
        // Material conditial table
        ("11>", true),
        ("01>", true),
        ("10>", false),
        ("11>", true),
        // Equivalence truth table
        ("00=", true),
        ("01=", false),
        ("10=", false),
        ("11=", true),
        // Andvanced tests
        ("1011||=", true),
    ];

    for &test in TESTS {
        assert_eq!(eval_formula(test.0), test.1, "Error on formula: {}", test.0);
    }
}
