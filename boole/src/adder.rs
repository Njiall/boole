pub fn adder(a: u32, b: u32) -> u32 {
	let mut carry = a;
	let mut result = b;
	while carry != 0 {
		let bwr = (carry & result) << 1;
		result = result ^ carry;
		carry = bwr;
	}
	return result;
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_adder() {
		const TESTS: &[(u32, u32)] = &[
			(2, 3),
			(15, 1),
			(23, 3),
			(2, 42),
			(30, 113123),
			(2, 3),
			(322313, 3221),
			(1312, 0),
			(2, 32123),
			(0, 3),
		];

		for &(a, b) in TESTS {
			assert_eq!(adder(a, b), a + b);
		}
	}
}
