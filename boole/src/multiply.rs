use crate::adder::adder;

#[allow(dead_code)]
pub fn multiply(num: u32, multiple: u32) -> u32 {
	let mut result = 0;
	let mut a = num;
	let mut b = multiple;

	while b != 0 {
		if b & 1 != 0 {
			result = adder(result, a);
		}
		a <<= 1;
		b >>= 1;
	}

	return result;
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_array() {
		const TESTS: &[(u32, u32)] = &[
			(2, 3),
			(15, 1),
			(23, 3),
			(2, 42),
			(30, 113123),
			(2, 3),
			(322313, 3221),
			(1312, 0),
			(2, 32123),
			(0, 3),
		];

		for &(a, b) in TESTS {
			assert_eq!(multiply(a, b), a * b);
		}
	}
}
