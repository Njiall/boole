#[allow(dead_code)]
pub fn gray_code(num: u32) -> u32 {
    return (num >> 1) ^ num;
}

#[allow(dead_code)]
fn is_gray_neighbor(a: u32, b: u32) -> u32{
    let mut count = 0;
    for i in 0..31 {
        if a & (1 << i) != b & (1 << i) {
            if count == 1 {
                return 2;
            }
            count += 1;
        }
    }
    return count;
}

#[test]
fn test_neighbour() {
    let test = |a: u32, b: u32, expect: u32| {
        let t = is_gray_neighbor(a, b);
        assert!(t == expect, "{:#b} -> {:#b} = {} != {}", a, b, t, expect);
    };

    test(0b10, 0b10, 0);
    test(0b00, 0b01, 1);
    test(0b10, 0b11, 1);
    test(0b100, 0b001, 2);
}

#[test]
#[ignore]
fn test_code() {
    let mut prev = gray_code(0);
    for i in 1..(1 << 31) {
        let next = gray_code(i);
        let valid = is_gray_neighbor(prev, next);
        assert!(valid == 1, "Failed to transition from {} ({:#b}) to {} ({:#b}), transitions: {}", i - 1, prev, i, next, valid);
        prev = next;
    }
}