use crate::gray_code::gray_code;

// Takes a set as input and outputs it's powerset.
// The powerset is the set with all the unique permutations of that set.
pub fn powerset(set: &[i32]) -> Vec<Vec<i32>> {
	// This is the powerset size which is all the permutations.
	// As one bit represent one entry in the set, we use 2 ^ set_len.
	let psize = 1 << set.len();

	let mut powerset = Vec::new();
	// Kind of ugly but type change can be weird.
	// We get the gray code of the set so we can make all of the combinations.
	for i in (0u32..psize as u32).map(|code| gray_code(code)) {
		let mut subset = Vec::new();
		// We scan bit by bit to know if we include original's set member.
		for idx in 0..set.len() {
			if i & (1 << idx) > 0 {
				subset.push(set[idx]);
			}
		}
		powerset.push(subset);
	}
	powerset
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::collections::HashSet;

	const TESTS: &[&[i32]] =
		&[&[], &[1], &[1, 2], &[1, 2, 3], &[1, 2, 3, 4], &[1, 2, 3, 4, 5]];

	#[test]
	fn is_powerset() {
		// Let x = set and y = powerset
		// To be a power set:
		// - size of y = 1 << sizeof x
		// - members of y are unique
		for test in TESTS {
			let powerset = powerset(test);
			assert_eq!(
                powerset.len(),
                1 << test.len(),
                "Not the right length:\n{plen} != 1 << {slen} for sets:\n{powerset:?}\nand\n{set:?}",
                plen = powerset.len(),
                slen = test.len(),
                powerset = powerset,
                set = test
            );
			println!("Powerset for set {:?} is of right size", test);
			let ulen = powerset.iter().collect::<HashSet<_>>().len();
			assert_eq!(ulen, powerset.len());
			println!("Powerset for set {:?} has unique members", test);
		}
	}
}
