use parser::parser::parse;
use parser::tools::nnf::apply_nnf;
use parser::tools::simplify::simplify;

pub fn nnf(format: &str) -> String {
    // Parse the input format
    let (ast, _) = match parse(format) {
        Ok(data) => data,
        Err(err) => panic!("{:?}", err),
    };
    // Convert to nnf ast
    let nnf = apply_nnf(&ast);
    // Convert to simpler ast
    let reduced = simplify(&nnf);
    // Generates the formula from ast
    reduced.into_formula()
    // nnf.into_formula()
}

#[cfg(test)]
mod test {
    use crate::nnf::nnf;
    use parser::parser::parse;
    use parser::tools::truth::Truth;

    const EXPRS: &[&str] = &[
        // Simple binop tests
        "AB|",
        "AB&",
        "AB^",
        "AB=",
        "AB>",
        // Simple pruning tests
        "AA|",
        "AA&",
        // Simple propagation tests
        "AB|!",
        "AB^!",
        "AB&!",
        // Advanced tests
        "AB>C|A!&D=!",
        // Hardcore tests
        "ABC||!",
        "ABC&|!",
        "ABC|&!",
        "ABC&&!",
        "A!B=C!|D&E!|F^!",
    ];

    #[test]
    fn truth_comp() {
        for test in EXPRS {
            let (rpn, rpn_vars) = match parse(test) {
                Ok(data) => data,
                Err(_) => panic!("Couldn't parse ast"),
            };
            let (nnf, nnf_vars) = match parse(&*nnf(test)) {
                Ok(data) => data,
                Err(_) => panic!("Couldn't parse ast"),
            };
            let trpn = Truth::new(&rpn_vars, &rpn);
            let tnnf = Truth::new(&nnf_vars, &nnf);
            assert_eq!(
                trpn, tnnf,
                "Truth tables are not the same.\n{}\n{}",
                trpn, tnnf
            );
        }
    }
}
